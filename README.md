# Commodity Crawler Web Application

Used Web Crawling , Web Scraping and Interactive Charts to build this application


## Tech Stack Used
- Python3
- Web Application Framework - Flask
- Libraries - Requests, BeautifulSoup4, Jquery, Ajax, HTML, CSS,D3.js,DC.js etc

### Requirements

- python >= 3.6
- requests
- beautifulsoup4
- Flask

## Get Started

### How to Install ( Better use Virtual env (venv) for testing this stuff )

## 1. Clone this repo and install the requirements
```
git clone
pip install -r requirements.txt
python3 web.py
```



### Author
Deepak Natanmai [gitlab.com/0xdebug](https://gitlab.com/0xdebug)