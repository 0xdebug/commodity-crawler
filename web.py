from flask import Flask, render_template, request
import requests
import json
from bs4 import BeautifulSoup
import sys
import re

import sqlite3

app = Flask(__name__)


@app.route('/')
def index():
	conn = sqlite3.connect('commodity.db')
	cr = conn.cursor()
	cr.execute('CREATE TABLE IF NOT EXISTS `statelist` (`state_name` TEXT NOT NULL,`state_id` TEXT NOT NULL UNIQUE)')
	cr.execute('CREATE TABLE IF NOT EXISTS `daily` (`district_id`	INTEGER,`market_name`	TEXT,`commodity_id`	INTEGER,`variety`	TEXT,`max_price`	TEXT,`min_price`	TEXT,`modal_price`	TEXT,`date`	TEXT,`state_id`	TEXT)')
	cr.execute('CREATE TABLE IF NOT EXISTS `commodities` (`commodity_name`	TEXT,`commodity_id`	TEXT)')
	return render_template('index.html')


@app.route('/addstate/<state_id>/<state_name>', methods=['GET', 'POST'])
def dailyfn(state_id,state_name):
	conn = sqlite3.connect('commodity.db')
	cr = conn.cursor()
	print('INSERT INTO statelist (`state_name`,`state_id`) VALUES ("'+state_name+'","'+state_id+'")')
	data = cr.execute('INSERT INTO statelist (`state_name`,`state_id`) VALUES ("'+state_name+'","'+state_id+'")')
	conn.commit()
	return 'success'

@app.route('/addveg/<veg_id>/<veg_name>', methods=['GET', 'POST'])
def addveg(veg_id,veg_name):
	conn = sqlite3.connect('commodity.db')
	cr = conn.cursor()
	data = cr.execute('INSERT INTO commodities (`commodity_name`,`commodity_id`) VALUES ("'+veg_name+'","'+veg_id+'")')
	conn.commit()
	return 'success'



@app.route('/statelist', methods=['GET', 'POST'])
def statelist():
	conn = sqlite3.connect('commodity.db')
	cr = conn.cursor()
	data = cr.execute('SELECT `state_name`,`state_id` from statelist')
	data = data.fetchall()
	return json.dumps(data)

@app.route('/draw/<state_name>', methods=['GET', 'POST'])
def draw(state_name):
	conn = sqlite3.connect('commodity.db')
	cr = conn.cursor()
	print(state_name)
	data = cr.execute('SELECT * from daily WHERE state_id = "'+state_name+'" ')
	data = data.fetchall()
	return json.dumps(data)

@app.route('/scrapetodb', methods=['GET', 'POST'])
def scrapetodb():
	conn = sqlite3.connect('commodity.db')
	cr = conn.cursor()
	cityList = cr.execute('SELECT `state_name`,`state_id` from statelist')
	cityList = cityList.fetchall()
	for current_city in cityList:
		print('Starting to scrape DB')
		print('Scraping city : '+str(current_city[1]))
		# print(current_city[1])
		date_str = '19-Oct-2019'

		commodityList = cr.execute('SELECT * from commodities')
		commodity_to_scrape = commodityList.fetchall()
		print(commodity_to_scrape)		
		op_str = ''
		for current_commodity in commodity_to_scrape:
			query = 'http://agmarknet.gov.in/SearchCmmMkt.aspx?Tx_Commodity='+str(current_commodity[1])+'&Tx_State='+str(current_city[1])+'&Tx_District=0&Tx_Market=0&DateFrom=19-Oct-2019&DateTo=19-Oct-2019&Fr_Date=19-Oct-2019&To_Date=19-Oct-2019&Tx_Trend=0&Tx_CommodityHead=0&Tx_StateHead=0&Tx_DistrictHead=--Select--&Tx_MarketHead=--Select--'
			# print(query)
			req = requests.get(query)
			# print(req.content)
			soup = BeautifulSoup(req.content, 'lxml')
			clean_data = soup.find(class_="tableagmark_new")
			val = ''
			daily_data = []
			# print(daily_data)
			cnt = 0
			# for each_row in clean_data:
			rows = clean_data.find_all('tr')
			rows.pop(0)

			for row in rows:
				
				daily_data.append([])
				cells = row.find_all(['th', 'td'])
				print(cells)
				if len(cells) > 1:
					db_state = cells[1].find('span').text.strip()
					db_market = cells[2].find('span').text.strip()
					db_commodity = cells[3].find('span').text.strip()
					db_variety = cells[4].find('span').text.strip()
					db_maxprice = cells[7].find('span').text.strip()
					db_minprice = cells[6].find('span').text.strip()
					db_modalprice = cells[8].find('span').text.strip()
					db_date = cells[9].find('span').text.strip()

					daily_data[cnt].append(db_state)
					daily_data[cnt].append(db_market)
					daily_data[cnt].append(db_commodity)
					daily_data[cnt].append(db_variety)
					daily_data[cnt].append(db_maxprice)
					daily_data[cnt].append(db_minprice)
					daily_data[cnt].append(db_modalprice)
					daily_data[cnt].append(db_date)
					daily_data[cnt].append(str(current_city[1]))
					cnt +=1

				if(cnt > 6):
					print('Broken at'+str(cnt))
					break

				# for cell in cells:
				# 	if(cell.find('span') is not None):
				# 		val = cell.find('span').text.strip()
				# 		print(val)
				# exit()
			# print(daily_data)
			if(cnt > 1):
				cr.executemany('INSERT INTO daily VALUES (?,?,?,?,?,?,?,?,?)', daily_data)
				conn.commit()
			else:
				print('No Data Found')

	return 'okkk'
	# conn = sqlite3.connect('commodity.db')
	# cr = conn.cursor()
	# data = cr.execute('SELECT `state_name`,`state_id` from statelist')	


# 

if __name__ == "__main__":
	app.run(debug=True)